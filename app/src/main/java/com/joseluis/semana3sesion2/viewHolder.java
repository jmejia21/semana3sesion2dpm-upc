package com.joseluis.semana3sesion2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.design.widget.Snackbar;

/**
 * Created by JoseLuis on 6/11/17.
 */

public class viewHolder extends RecyclerView.ViewHolder implements  View.OnLongClickListener{

    ImageView image;
    TextView nombre;
    TextView servicio;
    ItemLongClickListener  itemLongClickListener;




    public viewHolder(final View itemView){
        super(itemView);

        nombre = (TextView) itemView.findViewById(R.id.nombre);
        servicio = (TextView) itemView.findViewById(R.id.servicio);
        image = (ImageView) itemView.findViewById(R.id.imagen);

    }

    public void setItemLongClickListener(ItemLongClickListener ic){
        this.itemLongClickListener = ic;
    }

    @Override
    public boolean onLongClick(View view) {
        this.itemLongClickListener.onLongClick(getLayoutPosition());
        return false;
    }


}
