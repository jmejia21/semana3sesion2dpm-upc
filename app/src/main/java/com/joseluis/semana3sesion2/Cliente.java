package com.joseluis.semana3sesion2;

/**
 * Created by JoseLuis on 6/11/17.
 */

public class Cliente {

    private int imagen;
    private String nombre;
    private String servicio;

    public Cliente(int imagen, String nombre, String servicio) {
        this.imagen = imagen;
        this.nombre = nombre;
        this.servicio = servicio;
    }

    public String getServicio() {
        return servicio;
    }

    public String getNombre() {
        return nombre;
    }

    public int getImagen() {
        return imagen;
    }
}
