package com.joseluis.semana3sesion2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<Cliente> lstCliente = new ArrayList<Cliente>();
        lstCliente.add(new Cliente(R.drawable.cliente1,"Cliente 1","Servicio 1"));
        lstCliente.add(new Cliente(R.drawable.cliente2,"Cliente 2","Servicio 2"));
        lstCliente.add(new Cliente(R.drawable.cliente3,"Cliente 3","Servicio 3"));


        RecyclerView contenedor = (RecyclerView) findViewById(R.id.clientesRecyclerView);
        contenedor.setHasFixedSize(true);

        LinearLayoutManager layout = new LinearLayoutManager(getApplicationContext());
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        contenedor.setAdapter(new Adaptador(lstCliente));
        contenedor.setLayoutManager(layout);
    }
}
