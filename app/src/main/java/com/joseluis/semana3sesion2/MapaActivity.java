package com.joseluis.semana3sesion2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.List;

/**
 * Created by JoseLuis on 7/11/17.
 */

public class MapaActivity  extends AppCompatActivity {


    TextView empresaDescripcionTextView;
    TextView contactoDescripcionTextView;
    TextView direccionDescripcionTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapa_layout);

        empresaDescripcionTextView = (TextView) findViewById(R.id.empresaDescripcionTextView);
        contactoDescripcionTextView = (TextView) findViewById(R.id.contactoDescripcionTextView);
        direccionDescripcionTextView = (TextView) findViewById(R.id.direccionDescripcionTextView);



    }

}
