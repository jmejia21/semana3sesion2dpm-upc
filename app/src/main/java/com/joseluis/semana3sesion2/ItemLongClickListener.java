package com.joseluis.semana3sesion2;

/**
 * Created by JoseLuis on 7/11/17.
 */

public interface ItemLongClickListener {

    void onLongClick(int position );
}
