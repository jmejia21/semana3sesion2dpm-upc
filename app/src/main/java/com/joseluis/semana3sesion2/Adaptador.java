package com.joseluis.semana3sesion2;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by JoseLuis on 6/11/17.
 */

public class Adaptador extends RecyclerView.Adapter<viewHolder> {

    List<Cliente> lstCliente;
    String nombreCliente;

    public Adaptador(List<Cliente> lstCliente) {
        this.lstCliente=lstCliente;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout,parent,false);

        return new viewHolder(vista );
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {

        holder.nombre .setText(lstCliente.get(position).getNombre());
        holder.servicio.setText(lstCliente.get(position).getServicio());
        holder.image.setImageResource(lstCliente.get(position).getImagen() );

        // Long Click
        holder.setItemLongClickListener(new ItemLongClickListener() {
            @Override
            public void onLongClick(int position) {
                nombreCliente =lstCliente.get(position).getNombre();
            }
        });

    }

    @Override
    public int getItemCount() {
        return lstCliente.size() ;
    }
}
